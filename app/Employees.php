<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employees extends Authenticatable
{
    
    protected $table        = 'employees';

    protected $primaryKey   = 'id';

    protected $fillable = 
    [
        'first_name','last_name','company_id','email','phone'
    ];

}

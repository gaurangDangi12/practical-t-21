<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Console\Presets\Bootstrap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Mail;
use Config;
use Illuminate\Support\HtmlString;

class HomeController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        //Get Second dynamic.
    }
    /**
    * Home Page
    */
    public function index()
    {
        return view('home');   
    }
}


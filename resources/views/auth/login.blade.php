@extends('layouts.main')

@section('content')

    <!-- Start Slider Area -->
        <div class="login-area area-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="login-page">
                            <div class="login-form">
                                <h4 class="login-title text-center">LOG IN</h4>
                                @include('flash::message')
                                <div class="row">
                                    <form action="<?php echo route('login'); ?>" method="post">
                                        @csrf
                                        <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                            <label>E-Mail</label>
                                            <input type="text" id="email"  name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                                             @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                                            <label>Password</label>
                                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                            <input type="submit" id="submit" class="slide-btn login-btn" value="Login"/>
                                        </div> 
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                    </div>
                </div>
             </div>
        </div>
@endsection
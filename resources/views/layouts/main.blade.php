<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
       <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Practical Test</title>
        <!-- all css here -->
        <!-- bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="{{ url('/css/bootstrap.min.css') }}">

        <!-- style css -->
        <link rel="stylesheet" href="{{ url('/style.css') }}">

    <body>
        <header class="header-one">
            <!-- Start top bar -->
            <div class="topbar-area fix hidden-xs">
                <div class="container">
                   
                </div>
            </div>
            <!-- End top bar -->
            <!-- header-area start -->
            <div id="sticker" class="header-area header-area-3 hidden-xs">
                <div class="container">
                    <div class="row">
                        <!-- logo start -->
                        <div class="col-md-3 col-sm-3">
                            
                            <!-- logo end -->
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <!-- mainmenu start -->
                            <nav class="navbar navbar-default">
                                <div class="collapse navbar-collapse" id="navbar-example">
                                    <div class="main-menu">
                                        <ul class="nav navbar-nav navbar-right">
                                            @if(\Illuminate\Support\Facades\Auth::user())
                                                <li><a href="{{ url('/employees') }}">Employee</a></li>
                                                <li><a href="{{ url('/companies') }}">Companies</a></li>
                                                <li><a href="{{ url('/logout') }}">Logout</a></li>
                                            @endif
                                            @if(!(\Illuminate\Support\Facades\Auth::user()))
                                                <li><a class="s-menu" href="{{ url('/login') }}">Login</a></li>
                                                <li><a href="{{ url('/employees') }}">Employee</a></li>
                                                <li><a href="{{ url('/companies') }}">Companies</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- mainmenu end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-area end -->
            <!-- mobile-menu-area start -->
            <!-- mobile-menu-area end -->       
        </header>
        @if(isset($success) && $success!='')
            <div class="CustomAlert alert-success capitalizefont" role="alert" style="margin-top: 10px"> 
                  {{$success}}
            </div>
        @endif        
        @if(isset($error) && $error!='')
            <div class="CustomAlert alert-danger capitalizefont" role="alert">
                {!! $error !!}
            </div>  
        @endif
        
        @yield('content')

        
    <!--Footer-->
    <footer class="footer1">
        @yield('footerContent')

    </footer>
</body>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="{{ url('/js/bootstrap.min.js') }}"></script>    
<!-- jquery latest version -->
<script src="{{ url('/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- bootstrap js -->

@yield('pageBottomScriptSection')
@yield('editProjectReport')
</html>
